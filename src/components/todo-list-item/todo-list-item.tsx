import {Component, Event, Prop, State, h, EventEmitter} from '@stencil/core';

@Component({
  tag: 'todo-list-item',
  styleUrl: 'todo-list-item.css'
})
export class TodoListItem {

  @Prop() index: number;
  @Prop() item: any;
  @Prop() showButtons = true;
  @State() editing = false;

  value: string;
  debounce = false;

  @Event({
    eventName: 'todoEvent'
  }) todoEvent: EventEmitter;

  @Event({
    eventName: 'todoDragEvent'
  }) todoDragEvent: EventEmitter;

  input: HTMLIonInputElement;

  componentWillLoad() {

  }

  componentDidLoad() {
    this.value = this.item.content;
  }

  async componentDidRender() {
    if (this.editing) {
      await this.input.setFocus();
    }

    this.debounce = false;
  }

  async onEditClick() {
    if(this.editing) return;

    this.editing = true;
    //this.todoDragEvent.emit({ draggable: false });
    this.todoEvent.emit({ mode: 'editing', value: true });
  }

  async onDeleteClick() {
    if(this.editing) return;

    this.todoEvent.emit({mode: 'delete', index: this.index, item: this.item});
    this.todoDragEvent.emit({ draggable: true });
  }

  onInputChanged(event) {
    this.value = event.target.value;
  }

  onEditCompleted(event?) {
    if (event && event.key !== 'Enter') return;
    if (this.debounce) return;

    this.debounce = true;
    this.item = {...this.item, content: this.value};
    this.todoEvent.emit({mode: 'edit', index: this.index, item: this.item});
    this.todoDragEvent.emit({ draggable: true });
    this.todoEvent.emit({ mode: 'editing', value: false });
    this.editing = false;
  }

  onMouseEnterButton() {
    if(this.editing) return;

    this.todoDragEvent.emit({ draggable: false });
  }

  onMouseLeaveButton() {
    if(this.editing) return;

    this.todoDragEvent.emit({ draggable: true });
  }

  render() {
    if (!this.editing) {
      return (
        <ion-item>
          <ion-text class={this.showButtons ? '' : 'transparent-text'}>
            {`${this.index + 1}. ${this.item.content}`}
          </ion-text>
          { this.showButtons ? [
            <ion-button slot="end" fill="solid" size="default" expand="block"
                        onMouseEnter={() => this.onMouseEnterButton()}
                        onMouseLeave={() => this.onMouseLeaveButton()}
                        onMouseDown={() => this.onEditClick()}>
              Edit
              <ion-icon slot="icon-only" name="create"/>
            </ion-button>,
            <ion-button slot="end" fill="solid" size="default" expand="block"
                        onMouseEnter={() => this.onMouseEnterButton()}
                        onMouseLeave={() => this.onMouseLeaveButton()}
                        onMouseDown={() => this.onDeleteClick()}>
              Delete
              <ion-icon slot="icon-only" name="trash"/>
            </ion-button> ] : null }
        </ion-item>
      )
    } else {
      return (
        <ion-item>
          <ion-input ref={(el) => this.input = el as HTMLIonInputElement}
                     type="text" value={this.item.content}
                     onKeyUp={(evt) => {
                       this.onInputChanged(evt);
                       this.onEditCompleted(evt);
                       console.log('onKeyUp triggered')
                     }}
                     onIonBlur={() => this.onEditCompleted()}
          />
        </ion-item>
      )
    }
  }
}
