import {Component, State, h, Listen} from '@stencil/core';
import {ApiHelper} from "../../helpers/apiHelper";
import _ from 'underscore';

@Component({
  tag: 'todo-list',
  styleUrl: 'todo-list.css'
})
export class TodoList {

  @State() list: any[];
  @State() draggable = true;
  @State() editing = false;

  private apiHelper = new ApiHelper();
  private toastController: HTMLIonToastControllerElement;
  private newTodoInput: HTMLIonInputElement;
  private newTodoValue: string;

  @Listen('todoEvent')
  async handleTodoEvent(event: CustomEvent) {
    const {mode} = event.detail;

    if (mode === 'editing') {
      const {value} = event.detail;

      this.editing = value;
    } else {
      const {item, index} = event.detail;
      const {id, content, priority} = item;
      let newList = [...this.list];

      if (mode === 'edit') {
        newList[index] = item;
        await this.requestUpdateItem({id, content, priority});
      } else if (mode === 'delete') {
        newList.splice(index, 1);
        await this.requestDestroyItem({id});
        this.notify({message: 'Deleted todo item.'});
      }

      this.list = newList;
    }
  }

  @Listen('todoDragEvent')
  async handleTodoDragEvent(event: CustomEvent) {
    this.draggable = event.detail.draggable;
  }

  async componentWillLoad() {
    // this.list = [{id: 1, content: 'test1'}, {id: 2, content: 'test2'}, {id: 3, content: 'test3'}]
    const {data} = await this.apiHelper.request();
    if (!data) return;

    this.list = _.sortBy(data, 'priority');
  }

  componentDidLoad() {

  }

  async requestCreateItem({content, priority}) {
    return this.apiHelper.request({
      method: 'POST',
      data: {content, priority}
    })
  }

  async requestUpdateItem({id, content = null, priority = null}) {
    return this.apiHelper.request({
      url: ApiHelper.API_URL + id,
      method: 'PUT',
      data: {content, priority}
    })
  }

  async requestUpdateItems({items}) {
    return this.apiHelper.request({
      url: ApiHelper.API_URL,
      method: 'PUT',
      data: items
    })
  }

  async requestDestroyItem({id}) {
    return this.apiHelper.request({
      url: ApiHelper.API_URL + id,
      method: 'DELETE'
    })
  }

  async notify({message, duration = 2000}) {
    const toast = await this.toastController.create({
      message, duration
    });

    await toast.present();
  }

  async onKeyPressed(event) {
    this.newTodoValue = event.target.value;

    if (event.key === 'Enter') {
      this.onEnterPressed();
      return;
    }
  }

  async onEnterPressed() {
    const {data} = await this.requestCreateItem({content: this.newTodoValue, priority: this.list.length});
    if (!data) return;

    this.list = [...this.list, data];
    this.newTodoInput.value = '';
    this.newTodoValue = '';

    this.notify({message: 'Created new todo item.'});
  }

  async onItemReordered(event) {
    const {detail} = event;
    const result = await detail.complete(this.list as any);
    const payload = [];

    result.map((item, index) => {
      payload.push({id: item.id, priority: index});
      item.priority = index;
      return item;
    });

    const {data} = await this.requestUpdateItems({items: payload});
    if (!data) return;

    this.list = _.sortBy(data, 'priority');
  }

  renderListItems() {
    let index = 0;
    return this.list.map((item) => {
      return (
        <ion-reorder>
          <todo-list-item index={index++} item={item} showButtons={!this.editing}/>
        </ion-reorder>
      )
    })
  }

  render() {
    return [
      <ion-toast-controller ref={(el) => this.toastController = el} />,
      <ion-item>
        <ion-input ref={(el) => this.newTodoInput = el} type="text"
                   class="todo-input" minlength={1} maxlength={100}
                   placeholder="enter todo item and press enter"
                   onKeyPress={async (evt) => {
                     this.onKeyPressed(evt);
                   }}/>
      </ion-item>,

      <ion-reorder-group class="ion-no-padding" disabled={!this.draggable}
                         onIonItemReorder={(evt) => this.onItemReordered(evt)}>
        {this.renderListItems()}
      </ion-reorder-group>
    ]
  }
}
